package com.luk.fuelfinder.entities;

public class FillUp {

    private long id;
    private long stationId;
    private long userId;
    private int numberOfGallons;
    private int pricePerGallon;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getStationId() {
        return stationId;
    }

    public void setStationId(long stationId) {
        this.stationId = stationId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getNumberOfGallons() {
        return numberOfGallons;
    }

    public void setNumberOfGallons(int numberOfGallons) {
        this.numberOfGallons = numberOfGallons;
    }

    public int getPricePerGallon() {
        return pricePerGallon;
    }

    public void setPricePerGallon(int pricePerGallon) {
        this.pricePerGallon = pricePerGallon;
    }
}
